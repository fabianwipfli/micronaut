const path = require('path');

const mode = process.env.NODE_ENV || 'development';
const prod = mode === 'production';

module.exports = {
    entry: {
        bundle: ['./src/main.js']
    },
    resolve: {
        alias: {
            svelte: path.resolve('node_modules', 'svelte')
        },
        extensions: [".mjs", ".ts", ".tsx", ".js", ".json", ".svelte"],
        mainFields: ['svelte', 'browser', 'module', 'main']
    },
    output: {
        path: __dirname + '/public',
        filename: '[name].js',
        chunkFilename: '[name].[id].js'
    },
    module: {
        rules: [
            {
                test: /\.(js|ts|mjs|svelte)$/,
                use: {
                    loader: 'babel-loader'
                },
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.svelte$/,
                use: {
                    loader: 'svelte-loader',
                    options: {
                        emitCss: true,
                        hotReload: true
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000'
            }

        ]
    },
    mode,
    plugins: [],
    devtool: prod ? false : 'source-map',

    devServer: {
        port: 5000,
        proxy: {
            "/api": {
                target: "http://localhost:8081",
                changeOrigin: true,
                secure: false
            },
            "/login": {
                target: "http://localhost:8081",
                changeOrigin: true,
                secure: false
            }
        }
    }
};
