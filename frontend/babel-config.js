module.exports = function(api) {
    api.cache(true);

    const presets = [
        ["@babel/preset-env", {
            useBuiltIns: "entry", // or "entry"
            corejs: 2,
        }],
        "@babel/typescript",
    ];
    const plugins = [
        "@babel/proposal-class-properties",
        "@babel/proposal-object-rest-spread",
        "@babel/plugin-proposal-optional-chaining",
    ];

    return {
        comments: true,
        ignore: [/[\/\\]core-js/, /@babel[\/\\]runtime/],
        presets,
        plugins,
    };
};
