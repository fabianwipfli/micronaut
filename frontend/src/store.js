import {writable} from 'svelte/store'

export const env = {
    login: writable(""),
    location: writable(""),
};
