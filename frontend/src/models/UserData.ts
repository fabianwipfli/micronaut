export default class UserData {
    constructor(public email: string,
                public name: string,
                public firstname: string,
                public description: string,
                public street: string,
                public plz: string,
                public city: string,
                public latitude: string,
                public longitude: string,
                public createDateTime: string,
                public password?: string) {
    }
}
