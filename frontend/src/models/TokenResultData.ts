export default class TokenData {
    constructor(public username?: string, public access_token?: string, public refresh_token?: string, public expires_in?: number) {
    }
}


