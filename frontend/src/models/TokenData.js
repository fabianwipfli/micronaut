"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TokenData = /** @class */ (function () {
    function TokenData(token) {
        this.token = token;
    }
    return TokenData;
}());
exports.default = TokenData;
