export default class ProductData {
    constructor(public id?: string,
                public name?: string,
                public price?: string,
                public description?: string,
                public image?: string,
                public quantity?: string,
                public createDateTime?: string) {
    }
}

