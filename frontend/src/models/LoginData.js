"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoginData = /** @class */ (function () {
    function LoginData(email, password) {
        this.email = email;
        this.password = password;
    }
    return LoginData;
}());
exports.default = LoginData;
