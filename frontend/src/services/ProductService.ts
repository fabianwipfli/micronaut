import TokenData from "../models/TokenData";
import ProductData from "../models/ProductData";

export class ProductService {

    async getProducts(): Promise<Array<ProductData>>;
    async getProducts(token?: TokenData): Promise<Array<ProductData>> {
        const bearer = token ? "Bearer " + token : "";
        const response = await fetch(`/api/product`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': bearer
            }
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }

    async getProduct(id: String): Promise<Array<ProductData>> {
        const response = await fetch(`/api/product/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }

    async createProduct(data: ProductData, token: TokenData) {
        const response = await fetch(`/api/product`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + token
            },
            body: JSON.stringify(data)
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }

    async updateProduct(data: ProductData, token: TokenData) {
        const response = await fetch(`/api/product`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + token
            },
            body: JSON.stringify(data)
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }
}
