import LoginData from "../models/LoginData";
import TokenData from "../models/TokenData";
import TokenResultData from "../models/TokenResultData";
import UserData from "../models/UserData";

export class UserService {

    async login(data: LoginData): Promise<TokenResultData> {
        let response = await fetch(`/login`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }

    async comfirm(data: TokenData): Promise<TokenData> {
        let response = await fetch(`/api/profile/confirm`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }

    async getProfile(data: TokenData): Promise<Array<TokenData>> {
        const response = await fetch(`/api/profile`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': data.token
            }
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }

    async getProfiles(emailList: Array<String>): Promise<Array<TokenData>> {
        const params = encodeURI(emailList.join(","));
        const response = await fetch(`/api/profile?email=${params}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }

    async createProfile(data: UserData) {
        const response = await fetch(`/api/profile`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }

    async updateProfile(data: UserData, token: TokenData) {
        const response = await fetch(`/api/profile`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + token
            },
            body: JSON.stringify(data)
        });
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }
}
