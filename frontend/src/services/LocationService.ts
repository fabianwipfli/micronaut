import LoginData from "../models/LoginData";
import TokenData from "../models/TokenData";

export class LocationService {

    async getLocations(data: LoginData): Promise<Array<TokenData>> {
        const response = await fetch(`/api/location`);
        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(await response.json());
        }
    }
}
