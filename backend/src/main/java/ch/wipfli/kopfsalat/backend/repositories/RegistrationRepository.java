package ch.wipfli.kopfsalat.backend.repositories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import ch.wipfli.kopfsalat.backend.entities.RegistrationToken;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface RegistrationRepository extends CrudRepository<RegistrationToken, Long> {
    Optional<RegistrationToken> findByUser(UserProfile user);
    Optional<RegistrationToken> findByToken(String token);

    List<RegistrationToken> findByCreateDateTimeBefore(LocalDateTime creationDate);
}
