package ch.wipfli.kopfsalat.backend.controllers;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/ping")
public class PingController {

    @Get("/")
    public String index() {
        return "Hello World";
    }
}
