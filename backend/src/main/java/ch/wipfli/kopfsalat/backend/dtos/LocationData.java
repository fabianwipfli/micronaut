package ch.wipfli.kopfsalat.backend.dtos;

import ch.wipfli.kopfsalat.backend.entities.UserProfile;

public class LocationData {
    private String email;
    private String name;
    private String firstname;
    private String street;
    private String plz;
    private String city;
    private String latitude;
    private String longitude;

    public LocationData()
    {
    }

    public LocationData(UserProfile userProfile) {
        if (userProfile.getLocation() != null) {
            this.setCity(userProfile.getLocation().getTown());
            this.setLatitude(userProfile.getLocation().getLatitude());
            this.setLongitude(userProfile.getLocation().getLongitude());
            this.setPlz(userProfile.getLocation().getPostcode());
            this.setStreet(userProfile.getLocation().getStreet());
        }
        this.setEmail(userProfile.getEmail());
        this.setName(userProfile.getName());
        this.setFirstname(userProfile.getFirstname());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
