package ch.wipfli.kopfsalat.backend.exception;

public class UserAlreadyExistsException extends BaseException {
    public UserAlreadyExistsException() {
        super(ExceptionMessage.USER_ALREADY_EXISTS);
    }
}
