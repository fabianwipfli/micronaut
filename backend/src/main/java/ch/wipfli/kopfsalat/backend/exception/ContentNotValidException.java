package ch.wipfli.kopfsalat.backend.exception;

public class ContentNotValidException extends BaseException {
    public ContentNotValidException() {
        super(ExceptionMessage.CONTENT_NOT_VALID);
    }
}
