package ch.wipfli.kopfsalat.backend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import ch.wipfli.kopfsalat.backend.entities.Product;
import ch.wipfli.kopfsalat.backend.exception.ParamException;
import ch.wipfli.kopfsalat.backend.repositories.ProductBasketRepository;
import ch.wipfli.kopfsalat.backend.repositories.UserProfileRepository;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import ch.wipfli.kopfsalat.backend.exception.UserNotExistsException;


@Singleton
public class ProductServiceImpl implements ProductService {

    @Inject
    private UserProfileRepository userProfileRepository;

    @Inject
    private ProductBasketRepository productRepository;

    @Override
    public List<Product> getProducts(String email) {
        final Optional<UserProfile> userOpt = userProfileRepository.findById(email);
        if (userOpt.isPresent()) {
            return userOpt.get().getProducts();
        }
        return new ArrayList<>();
    }

    @Override
    @Transactional
    public List<Product> addProduct(String email, Product product) {
        Optional<UserProfile> userOpt = userProfileRepository.findById(email);
        if (userOpt.isPresent()) {
            productRepository.save(product);
            return userOpt.get().addProduct(product);
        }
        return new ArrayList<>();
    }

    @Override
    @Transactional
    public Product editProduct(Product product) {
            return productRepository.save(product);
    }

    @Override
    @Transactional
    public List<Product> removeProduct(String email, Product product) {
        List<Product> result = new ArrayList<>();
        final Optional<UserProfile> userOpt = userProfileRepository.findById(email);
        if (userOpt.isPresent()) {
            result = userOpt.get().removeProduct(product);
            productRepository.delete(product);
            return result;
        }
        return result;
    }

    @Override
    public Product findProduct(String email, Long id) throws UserNotExistsException {
        final Optional<UserProfile> userOpt = userProfileRepository.findById(email);
        if (userOpt.isPresent()) {
            return userOpt.get().getProducts().stream().filter(f -> f.getId().equals(id)).findFirst().get();
        }
        throw new UserNotExistsException();
    }

    @Override
    public Product findProduct(Long id) throws ParamException {
        final Optional<Product> result = productRepository.findById(id);
        if(result.isPresent())
        {
            return result.get();
        }
        throw new ParamException();
    }
}
