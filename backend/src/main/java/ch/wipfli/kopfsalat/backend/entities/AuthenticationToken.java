package ch.wipfli.kopfsalat.backend.entities;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class AuthenticationToken extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String token;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "email")
    private UserProfile user;

    public AuthenticationToken(UserProfile user) {
        this.user = user;
        token = UUID.randomUUID().toString();
    }

    public AuthenticationToken() {
    }

    public UserProfile getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    public long getId() {
        return id;
    }
}
