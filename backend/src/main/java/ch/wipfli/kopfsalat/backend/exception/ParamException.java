package ch.wipfli.kopfsalat.backend.exception;

public class ParamException extends BaseException {
    public ParamException() {
        super(ExceptionMessage.PARAM_NOT_VALID);
    }
}
