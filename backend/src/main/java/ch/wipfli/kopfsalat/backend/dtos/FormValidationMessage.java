package ch.wipfli.kopfsalat.backend.dtos;

import java.util.Date;
import java.util.List;

public class FormValidationMessage {
        private Date timestamp;
        private String message;
        private String details;
        private List<FormValidation> forms;

        public FormValidationMessage(Date timestamp, String message, String details, List<FormValidation> forms) {
            super();
            this.timestamp = timestamp;
            this.message = message;
            this.details = details;
            this.forms = forms;
        }
}
