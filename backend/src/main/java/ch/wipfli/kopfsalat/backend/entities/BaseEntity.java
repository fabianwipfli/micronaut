package ch.wipfli.kopfsalat.backend.entities;

import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import io.micronaut.data.annotation.DateCreated;
import io.micronaut.data.annotation.DateUpdated;

@MappedSuperclass
public abstract class BaseEntity {

    @DateCreated
    private LocalDateTime createDateTime;

    private String createdBy;

    @DateUpdated
    private LocalDateTime updateDateTime;

    private String updatedBy;

    @Version
    private Integer version;

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Integer getVersion() {
        return version;
    }

    public boolean isPersisted() { return version != null; }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    //public abstract Throwable getUserName();
}
