package ch.wipfli.kopfsalat.backend.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ShoppingCart extends BaseEntity {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }

    public List<Product> addProduct(Product product)
    {
        this.productList.add(product);
        return this.productList;
    }

    public List<Product> removeProduct(Product product)
    {
        this.productList.remove(product);
        return this.productList;
    }
}
