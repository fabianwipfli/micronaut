package ch.wipfli.kopfsalat.backend.dtos;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

import ch.wipfli.kopfsalat.backend.entities.Product;


public class ProductData {

    private static final String FORMAT = "dd.MM.yyyy";

    private Long id;
    private String name;
    private BigDecimal price;
    private String description;
    private String image;
    private int quantity;
    private String createDateTime;

    public ProductData() {

    }

    public ProductData(Product product) {
        setDescription(product.getProductDescription().getDescription());
        setId(product.getId());
        setPrice(product.getProductDescription().getPrice());
        setQuantity(product.getQuantity());
        setName(product.getProductDescription().getName());

        byte[] image = product.getProductDescription().getImage();
        if (image != null) {
            setImage(new String (image));
        }

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
        setCreateDateTime(product.getCreateDateTime().format(formatter));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
            return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCreateDateTime()
    {
        return this.createDateTime;
    }

    public void setCreateDateTime(String createDateTime)
    {
        this.createDateTime = createDateTime;
    }
}
