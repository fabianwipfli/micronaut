package ch.wipfli.kopfsalat.backend.service;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.mail.EmailException;

import ch.wipfli.kopfsalat.backend.exception.ContentNotValidException;
import ch.wipfli.kopfsalat.backend.exception.EmailNotValidException;
import ch.wipfli.kopfsalat.backend.exception.TokenNotValidException;
import ch.wipfli.kopfsalat.backend.exception.UserAlreadyExistsException;
import ch.wipfli.kopfsalat.backend.exception.UserNotAuhorisedException;
import ch.wipfli.kopfsalat.backend.repositories.AuthenticationRepository;
import ch.wipfli.kopfsalat.backend.repositories.RegistrationRepository;
import ch.wipfli.kopfsalat.backend.repositories.UserProfileRepository;
import ch.wipfli.kopfsalat.backend.entities.AuthenticationToken;
import ch.wipfli.kopfsalat.backend.entities.RegistrationToken;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import ch.wipfli.kopfsalat.backend.exception.UserNotExistsException;
import io.micronaut.security.authentication.providers.PasswordEncoder;

@Singleton
public class UserProfileServiceImpl implements UserProfileService {

    @Inject
    private UserProfileRepository userProfileRepository;

    @Inject
    private RegistrationRepository registrationRepository;

    @Inject
    private AuthenticationRepository loginRepository;

    @Inject
    private EmailSenderService emailSenderService;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Override
    public AuthenticationToken login(String username, String password) throws UserNotAuhorisedException {
        final Optional<UserProfile> userProfile = userProfileRepository.findById(username);
        if (userProfile.isPresent() && passwordEncoder.matches(password, userProfile.get().getPassword()) && userProfile.get().isEnabled()) {
            //Delete exists
            Optional<AuthenticationToken> existToken = loginRepository.findByUser(userProfile.get());
            existToken.ifPresent(t -> loginRepository.delete(t));

            //Create new token
            final AuthenticationToken authToken = new AuthenticationToken(userProfile.get());
            loginRepository.save(authToken);
            return authToken;
        }
        throw new UserNotAuhorisedException();
    }

    @Override
    public UserProfile logout(String username) throws UserNotExistsException {
        final Optional<UserProfile> userProfile = userProfileRepository.findById(username);
        if (userProfile.isPresent()) {
            final Optional<AuthenticationToken> token = loginRepository.findByUser(userProfile.get());
            token.ifPresent(authenticationToken -> loginRepository.delete(authenticationToken));
            return userProfile.get();
        }
        throw new UserNotExistsException();
    }

    @Override
    public boolean isExpired(String username) {
        int minutes = 60;

        final Optional<AuthenticationToken> token = userProfileRepository
                .findById(username)
                .flatMap(f -> loginRepository.findByUser(f));

        if (token.isPresent()) {
            LocalDateTime dateTimeBefore = LocalDateTime.now().minusMinutes(minutes);
            return token.get().getCreateDateTime().isBefore(dateTimeBefore);
        }
        return false;
    }

    @Override
    public UserProfile register(UserProfile userProfile, String requestUrl) throws ContentNotValidException, UserAlreadyExistsException, EmailNotValidException {
        try {
            if (userProfile.getPassword() == null || userProfile.getEmail() == null) {
                throw new ContentNotValidException();
            }
            final Optional<UserProfile> existsUser = userProfileRepository.findById(userProfile.getEmail());
            if (existsUser.isPresent()) {
                throw new UserAlreadyExistsException();
            }

            final String token = UUID.randomUUID().toString();
            final String url = MessageFormat.format("{0}/confirmation/{1}", requestUrl.replace("/register", ""), token);
            emailSenderService.sendEmail(
                    userProfile, "Complete Registration",
                    MessageFormat.format("<html><body>To confirm your account, please click here : <a href=\"{0}\">{1}</a>\n</body></html>", url, url)
            );

            userProfile.setPassword(passwordEncoder.encode(userProfile.getPassword()));

            final UserProfile result = userProfileRepository.save(userProfile);
            RegistrationToken confirmationToken = new RegistrationToken(userProfile, token);
            registrationRepository.save(confirmationToken);
            return result;
        } catch (EmailException e) {
            throw new EmailNotValidException();
        }
    }

    @Override
    public UserProfile update(UserProfile userProfile) throws ContentNotValidException {
        if (userProfile.getPassword() == null || userProfile.getEmail() == null) {
            throw new ContentNotValidException();
        }
        final UserProfile result = userProfileRepository.save(userProfile);
        return result;
    }

    @Override
    public UserProfile registerWithoutConfirm(UserProfile userProfile) throws UserAlreadyExistsException, ContentNotValidException {
        if (userProfile.getEmail() == null || userProfile.getPassword() == null) {
            throw new ContentNotValidException();
        }
        final Optional<UserProfile> existsUser = userProfileRepository.findById(userProfile.getEmail());
        if (existsUser.isPresent()) {
            throw new UserAlreadyExistsException();
        }
        userProfile.setEnabled(true);
        userProfile.setPassword(passwordEncoder.encode(userProfile.getPassword()));

        return userProfileRepository.save(userProfile);
    }

    @Override
    public UserProfile resetPassword(String username, String newPassword) throws UserNotAuhorisedException {
        final Optional<UserProfile> user = userProfileRepository.findById(username);

        if (!user.isPresent()) {
            throw new UserNotAuhorisedException();
        }
        user.get().setPassword(passwordEncoder.encode(newPassword));
        userProfileRepository.save(user.get());
        return user.get();
    }

    @Override
    public UserProfile forgotPassword(String username) throws UserNotExistsException, EmailException {
        final UserProfile user = findById(username);
        final String newPassword = new Random().ints(10, 33, 122).collect(StringBuilder::new,
                StringBuilder::appendCodePoint, StringBuilder::append).toString();

        user.setPassword(passwordEncoder.encode(newPassword));
        final UserProfile result = userProfileRepository.save(user);

        emailSenderService.sendEmail(user, "New password", "The new password will be " + newPassword);

        return result;
    }

    @Override
    public UserProfile confirm(String confirmationToken) throws TokenNotValidException {
        final Optional<RegistrationToken> token = registrationRepository.findByToken(confirmationToken);

        if (token.isPresent()) {
            UserProfile user = token.get().getUser();
            user.setEnabled(true);
            userProfileRepository.save(user);
            registrationRepository.delete(token.get());
            return user;
        }
        throw new TokenNotValidException();
    }

    @Override
    public UserProfile findByToken(String token) throws UserNotExistsException {
        final Optional<AuthenticationToken> loginToken = loginRepository.findByToken(token);
        final Optional<UserProfile> user = loginToken.map(AuthenticationToken::getUser);
        if (user.isPresent()) {
            return user.get();
        }
        throw new UserNotExistsException();
    }

    @Override
    public UserProfile findById(String id) throws UserNotExistsException {
        final Optional<UserProfile> user = userProfileRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        }
        throw new UserNotExistsException();
    }

    @Override
    public List<UserProfile> findAll() {
        final List<UserProfile> list = new ArrayList<>();
        userProfileRepository.findAll().forEach(list::add);
        return list;
    }

    @Override
    public void delete(String id) throws UserNotExistsException {
        final Optional<UserProfile> user = userProfileRepository.findById(id);
        user.ifPresent(userProfile -> userProfileRepository.delete(userProfile));
        throw new UserNotExistsException();
    }
}
