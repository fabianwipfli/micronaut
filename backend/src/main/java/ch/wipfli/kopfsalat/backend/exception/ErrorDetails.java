package ch.wipfli.kopfsalat.backend.exception;

import java.time.LocalDateTime;


public class ErrorDetails {
    private int messageId;
    private LocalDateTime timestamp;
    private String message;
    private String details;

    public ErrorDetails(int messageId, LocalDateTime timestamp, String message, String details) {
        super();
        this.messageId = messageId;
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
