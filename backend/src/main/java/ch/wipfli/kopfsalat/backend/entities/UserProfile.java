package ch.wipfli.kopfsalat.backend.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class UserProfile extends BaseEntity {

    @Id
    private String email;
    @NotNull
    private String name;
    @NotNull
    private String firstname;
    @NotNull
    private String password;
    private boolean enabled;
    @OneToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private Location location;
    @OneToMany(fetch = FetchType.LAZY)
    private List<Product> productList;
    @OneToOne(fetch = FetchType.LAZY)
    private ShoppingCart shoppingCart;
    private String description;
    @Lob
    @NotNull
    private byte[] image;

    public UserProfile(String email, String password, String name, String firstname) {
        this.email = email;
        this.name = name;
        this.firstname = firstname;
        this.password = password;
    }

    public UserProfile(){}

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Product> getProducts() {
        return this.productList;
    }

    public List<Product> addProduct(Product product) {
        this.productList.add(product);
        return this.productList;
    }

    public List<Product> removeProduct(Product product) {
        this.productList.remove(product);
        return this.productList;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
