package ch.wipfli.kopfsalat.backend.exception;

public class EmailNotValidException extends BaseException {
    public EmailNotValidException() {
        super(ExceptionMessage.EMAIL_NOT_VALID);
    }
}
