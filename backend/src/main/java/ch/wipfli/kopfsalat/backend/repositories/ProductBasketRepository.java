package ch.wipfli.kopfsalat.backend.repositories;

import ch.wipfli.kopfsalat.backend.entities.Product;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface ProductBasketRepository extends CrudRepository<Product, Long> {
}
