package ch.wipfli.kopfsalat.backend.service;

import java.util.List;

import ch.wipfli.kopfsalat.backend.entities.Product;
import ch.wipfli.kopfsalat.backend.exception.ParamException;
import ch.wipfli.kopfsalat.backend.exception.UserNotExistsException;

public interface ProductService {

    List<Product> getProducts(String email);

    List<Product> addProduct(String email, Product product);

    Product editProduct(Product product);

    List<Product> removeProduct(String email, Product product);

    Product findProduct(String email, Long id) throws UserNotExistsException;

    Product findProduct(Long id) throws ParamException;
}
