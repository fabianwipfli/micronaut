package ch.wipfli.kopfsalat.backend.exception;

public class TokenNotValidException extends BaseException {
    public TokenNotValidException() {
        super(ExceptionMessage.TOKEN_NOT_VALID);
    }
}
