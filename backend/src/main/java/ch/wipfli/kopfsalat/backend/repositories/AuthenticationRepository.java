package ch.wipfli.kopfsalat.backend.repositories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import ch.wipfli.kopfsalat.backend.entities.AuthenticationToken;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface AuthenticationRepository extends CrudRepository<AuthenticationToken, Long> {

    Optional<AuthenticationToken> findByUser(UserProfile user);
    Optional<AuthenticationToken> findByToken(String token);

    List<AuthenticationToken> findByCreateDateTimeBefore(LocalDateTime creationDate);
}
