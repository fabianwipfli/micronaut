package ch.wipfli.kopfsalat.backend.service;

import javax.inject.Singleton;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import io.micronaut.context.annotation.Value;
import io.micronaut.scheduling.annotation.Async;

@Singleton
public class EmailSenderServiceImpl implements EmailSenderService {

    @Value("${mail.host}")
    private String host;

    @Value("${mail.port}")
    private String port;

    @Value("${mail.username}")
    private String username;

    @Value("${mail.password}")
    private String password;

    @Override
    @Async
    public void sendEmail(UserProfile userProfile, String subject, String html) throws EmailException {
        final HtmlEmail email = new HtmlEmail();
        email.setHostName(host);
        email.setSmtpPort(Integer.parseInt(port));
        email.setSSLOnConnect(true);
        email.setAuthentication(username, password);

        email.setFrom(username);
        email.addTo(userProfile.getEmail(), userProfile.getName());
        email.setSubject(subject);
        email.setHtmlMsg(html);

        email.send();
    }
}
