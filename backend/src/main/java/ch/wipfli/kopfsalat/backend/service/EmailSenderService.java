package ch.wipfli.kopfsalat.backend.service;

import org.apache.commons.mail.EmailException;

import ch.wipfli.kopfsalat.backend.entities.UserProfile;

public interface EmailSenderService {

    void sendEmail(UserProfile userProfile, String subject, String text) throws EmailException;
}
