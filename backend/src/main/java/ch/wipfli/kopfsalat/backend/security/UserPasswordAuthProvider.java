package ch.wipfli.kopfsalat.backend.security;

import java.util.ArrayList;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;

import ch.wipfli.kopfsalat.backend.repositories.UserProfileRepository;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import io.micronaut.security.authentication.AuthenticationFailed;
import io.micronaut.security.authentication.AuthenticationProvider;
import io.micronaut.security.authentication.AuthenticationRequest;
import io.micronaut.security.authentication.AuthenticationResponse;
import io.micronaut.security.authentication.UserDetails;
import io.reactivex.Flowable;
import org.reactivestreams.Publisher;

@Singleton
public class UserPasswordAuthProvider implements AuthenticationProvider {

    @Inject
    private UserProfileRepository userProfileRepository;

    @Override
    public Publisher<AuthenticationResponse> authenticate(AuthenticationRequest req) {
        final String username = req.getIdentity().toString();
        final String password = req.getSecret().toString();
        final Optional<UserProfile> user = userProfileRepository.login(username, password);
        if(user.isPresent())
        {
        final UserDetails details = new UserDetails(username, new ArrayList<>());
            return Flowable.just(details);
        } else {
            return Flowable.just(new AuthenticationFailed());
        }
    }
}
