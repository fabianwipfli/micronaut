package ch.wipfli.kopfsalat.backend.controllers;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.transaction.Transactional;

import ch.wipfli.kopfsalat.backend.dtos.LocationData;
import ch.wipfli.kopfsalat.backend.entities.Location;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import ch.wipfli.kopfsalat.backend.exception.UserNotExistsException;
import ch.wipfli.kopfsalat.backend.service.UserProfileService;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;

@Secured(SecurityRule.IS_ANONYMOUS)
@Controller("/api/location")
@Transactional
public class LocationController {

    @Inject
    private UserProfileService userProfileService;

    @Get("/{id}")
    public Location getLocation(@PathVariable String id) throws UserNotExistsException {
        final UserProfile profile = userProfileService.findById(id);
        return profile.getLocation();
    }

    @Get("/")
    public List<LocationData> getLocations() {
        final List<UserProfile> profile = userProfileService.findAll();
        final List<LocationData> data = profile.stream().map(LocationData::new).collect(Collectors.toList());
        return data;
    }
}

