package ch.wipfli.kopfsalat.backend.exception;

import javax.inject.Singleton;
import javax.validation.ConstraintViolationException;

import io.micronaut.validation.exceptions.ConstraintExceptionHandler;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

@Produces
@Singleton
@Requires(classes = {ConstraintViolationException.class, ExceptionHandler.class})
public class ConstraintViolationExceptionHandler implements ExceptionHandler<ConstraintViolationException, HttpResponse> {

    @Override
    public HttpResponse handle(HttpRequest request, ConstraintViolationException exception) {
        return HttpResponse.badRequest(exception.getConstraintViolations());
    }
}
