package ch.wipfli.kopfsalat.backend.exception;


public class UserNotAuhorisedException extends BaseException {
    public UserNotAuhorisedException() {
        super(ExceptionMessage.USER_NOT_AUTHORISED);
    }
}
