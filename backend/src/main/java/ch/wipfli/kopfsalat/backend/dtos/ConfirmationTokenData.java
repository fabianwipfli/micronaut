package ch.wipfli.kopfsalat.backend.dtos;

public class ConfirmationTokenData {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
