package ch.wipfli.kopfsalat.backend.dtos;

import java.time.format.DateTimeFormatter;

import javax.validation.constraints.NotNull;

import ch.wipfli.kopfsalat.backend.entities.UserProfile;

public class BenutzerData {

    private static final String FORMAT = "dd.MM.yyyy";

    private String email;
    private String name;
    private String firstname;
    private String password;
    private String description;
    private String street;
    private String plz;
    private String city;
    private String latitude;
    private String longitude;
    private String createDateTime;
    @NotNull
    private String image;

    public BenutzerData()
    {
    }

    public BenutzerData(UserProfile userProfile) {
        if(userProfile.getLocation() != null) {
            this.setCity(userProfile.getLocation().getTown());
            this.setLatitude(userProfile.getLocation().getLatitude());
            this.setLongitude(userProfile.getLocation().getLongitude());
            this.setPlz(userProfile.getLocation().getPostcode());
            this.setStreet(userProfile.getLocation().getStreet());
        }
        this.setImage(new String(userProfile.getImage()));
        this.setEmail(userProfile.getEmail());
        this.setName(userProfile.getName());
        this.setFirstname(userProfile.getFirstname());
        this.setDescription(userProfile.getDescription());
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
        this.setCreateDateTime(userProfile.getCreateDateTime().format(formatter));
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description == null ? "" : description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreateDateTime()
    {
        return this.createDateTime;
    }

    public void setCreateDateTime(String createDateTime)
    {
        this.createDateTime = createDateTime;
    }
}
