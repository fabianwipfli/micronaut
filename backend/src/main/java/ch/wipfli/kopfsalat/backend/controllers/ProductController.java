package ch.wipfli.kopfsalat.backend.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;

import ch.wipfli.kopfsalat.backend.exception.ParamException;
import ch.wipfli.kopfsalat.backend.dtos.ProductData;
import ch.wipfli.kopfsalat.backend.entities.Product;
import ch.wipfli.kopfsalat.backend.entities.ProductDescription;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import ch.wipfli.kopfsalat.backend.exception.UserNotExistsException;
import ch.wipfli.kopfsalat.backend.service.ProductService;
import ch.wipfli.kopfsalat.backend.service.UserProfileService;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;

@Controller("/api/product")
@Transactional
public class ProductController {

    @Inject
    private UserProfileService userProfileService;

    @Inject
    private ProductService productService;

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/")
    public List<ProductData> getProducts(@Nullable @QueryValue(value = "email")  String[] emails, @Nullable Principal principal) throws UserNotExistsException {
        final List<Product> result = new ArrayList<>();
        UserProfile authorizedUser = null;

        if(principal != null)
        {
            authorizedUser = userProfileService.findById(principal.getName());
            result.addAll(productService.getProducts(authorizedUser.getEmail()));
        }
        if(emails != null) {
            for (String email : emails) {
                final List<Product> basketList = productService.getProducts(email);
                result.addAll(basketList);
            }
        }
        final List<ProductData> resultList = new ArrayList<>();
        for (Product item : result) {
            resultList.add(new ProductData(item));
        }
        return resultList;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Post("/")
    public List<ProductData> addProduct(Principal principal, @Valid @Body ProductData productData) throws UserNotExistsException {
        final List<ProductData> resultList = new ArrayList<>();
        final UserProfile user = userProfileService.findById(principal.getName());
        final ProductDescription productDescription = new ProductDescription();
        if(productData.getImage() != null) {
            productDescription.setImage(productData.getImage().getBytes());
        }
        productDescription.setDescription(productData.getDescription());
        productDescription.setName(productData.getName());
        productDescription.setPrice(productData.getPrice());
        final Product product = new Product(productData.getQuantity(), productDescription);
        final List<Product> productList = productService.addProduct(user.getEmail(), product);
        for (Product item : productList) {
            resultList.add(new ProductData(item));
        }
        return resultList;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Put("/")
    public ProductData editProduct(Principal principal, @Valid @Body ProductData productData) throws UserNotExistsException {
        final UserProfile user = userProfileService.findById(principal.getName());
        final Product product = productService.findProduct(user.getEmail(), productData.getId());
        final ProductDescription productDescription = product.getProductDescription();
        if(productData.getImage() != null) {
            productDescription.setImage(productData.getImage().getBytes());
        }
        productDescription.setDescription(productData.getDescription());
        productDescription.setName(productData.getName());
        productDescription.setPrice(productData.getPrice());
        product.setQuantity(productData.getQuantity());
        productService.editProduct(product);
        return new ProductData(product);
    }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/{id}")
    public ProductData getProduct(@PathVariable Long id) throws ParamException {
        final Product product = productService.findProduct(id);
        return new ProductData(product);
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Delete("/{id}")
    public List<ProductData> removeProduct(Principal principal, @PathVariable Long id) throws UserNotExistsException {
        final List<ProductData> resultList = new ArrayList<>();
        final UserProfile user = userProfileService.findById(principal.getName());
        final Product product = productService.findProduct(user.getEmail(), id);
        final List<Product> productBasketList = productService.removeProduct(user.getEmail(), product);
        for(Product item : productBasketList)
        {
            resultList.add(new ProductData(item));
        }
        return resultList;
    }
}

