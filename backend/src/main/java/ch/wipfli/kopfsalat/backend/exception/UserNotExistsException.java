package ch.wipfli.kopfsalat.backend.exception;

public class UserNotExistsException extends BaseException {
    public UserNotExistsException() {
        super(ExceptionMessage.USER_NOT_EXISTS);
    }
}
