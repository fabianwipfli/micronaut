package ch.wipfli.kopfsalat.backend.exception;

public enum ExceptionMessage {

    USER_ALREADY_EXISTS(1, "User already exists."),
    USER_NOT_AUTHORISED(2, "User is not authorised."),
    EMAIL_NOT_VALID(3, "Email is not valid."),
    CONTENT_NOT_VALID(4, "Content is not valid."),
    AUTHENTICATION_TOKEN_EXPIRED(5, "Authentication token is expired."),
    USER_NOT_EXISTS(6, "User not exists."),
    TOKEN_NOT_VALID(7, "Token is not valid."),
    PARAM_NOT_VALID(8, "Parameter not valid.");

    private int id;
    private String message;

    ExceptionMessage(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
