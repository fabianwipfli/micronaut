package ch.wipfli.kopfsalat.backend.repositories;

import java.util.List;
import java.util.Optional;

import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

@Repository
public interface UserProfileRepository extends CrudRepository<UserProfile, String> {

    @Query(value = "SELECT u FROM UserProfile u where u.email = :username and u.password = :password ")
    Optional<UserProfile> login(String username, String password);
}
