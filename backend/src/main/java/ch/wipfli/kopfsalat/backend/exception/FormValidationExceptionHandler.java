package ch.wipfli.kopfsalat.backend.exception;

import java.util.Date;

import javax.inject.Singleton;

import ch.wipfli.kopfsalat.backend.dtos.FormValidationMessage;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

@Produces
@Singleton
@Requires(classes = {FormValidationException.class, ExceptionHandler.class})
public class FormValidationExceptionHandler implements ExceptionHandler<FormValidationException, HttpResponse> {

    @Override
    public HttpResponse handle(HttpRequest request, FormValidationException exception) {
        final FormValidationMessage formValidationMessage = new FormValidationMessage(new Date(), exception.getMessage(),
                null, exception.getFormValidationList());
        return HttpResponse.badRequest(formValidationMessage);
    }
}
