package ch.wipfli.kopfsalat.backend.service;

import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.mail.EmailException;

import ch.wipfli.kopfsalat.backend.exception.ContentNotValidException;
import ch.wipfli.kopfsalat.backend.exception.EmailNotValidException;
import ch.wipfli.kopfsalat.backend.exception.TokenNotValidException;
import ch.wipfli.kopfsalat.backend.exception.UserAlreadyExistsException;
import ch.wipfli.kopfsalat.backend.exception.UserNotAuhorisedException;
import ch.wipfli.kopfsalat.backend.entities.AuthenticationToken;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import ch.wipfli.kopfsalat.backend.exception.UserNotExistsException;

public interface UserProfileService {

    AuthenticationToken login(String username, String password) throws UserNotAuhorisedException;

    UserProfile logout(String username) throws UserNotExistsException;

    boolean isExpired(String username);

    UserProfile register(UserProfile userProfile, String requestUrl) throws ContentNotValidException, UserAlreadyExistsException, EmailNotValidException;

    UserProfile update(UserProfile userProfile) throws ContentNotValidException;

    UserProfile registerWithoutConfirm(UserProfile userProfile) throws UserAlreadyExistsException, ContentNotValidException;

    UserProfile resetPassword(String username, String newPassword) throws UserNotAuhorisedException;

    UserProfile forgotPassword(String username) throws UserNotExistsException, MessagingException, EmailException;

    UserProfile confirm(String confirmationToken) throws TokenNotValidException;

    UserProfile findByToken(String token) throws UserNotExistsException;

    UserProfile findById(String id) throws UserNotExistsException;

    List<UserProfile> findAll();
    void delete(String id) throws UserNotExistsException;
}
