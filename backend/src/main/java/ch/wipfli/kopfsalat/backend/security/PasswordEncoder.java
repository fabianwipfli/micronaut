package ch.wipfli.kopfsalat.backend.security;

import javax.inject.Singleton;

@Singleton
public class PasswordEncoder implements io.micronaut.security.authentication.providers.PasswordEncoder {

    @Override
    public String encode(String rawPassword) {
        return rawPassword;
    }

    @Override
    public boolean matches(String rawPassword, String encodedPassword) {
        return rawPassword.equals(encodedPassword);
    }
}
