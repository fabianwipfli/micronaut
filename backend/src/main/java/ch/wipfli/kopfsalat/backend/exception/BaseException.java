package ch.wipfli.kopfsalat.backend.exception;

public abstract class BaseException extends Exception {

    private int id;

    public BaseException(ExceptionMessage message) {
        super(message.getMessage());
        this.id = message.getId();
    }

    public int getId() {
        return id;
    }
}
