package ch.wipfli.kopfsalat.backend.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Product extends BaseEntity {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    private ProductDescription productDescription;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ProductItem> productList;

    private Product(){
    }

    public Product(int quantity, ProductDescription productDescription) {
        productList = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            final ProductItem product = new ProductItem();
            this.productDescription = productDescription;
            productList.add(product);
        }
    }

    public List<ProductItem> add() {
        final ProductItem product = new ProductItem();
        productList.add(product);
        return productList;
    }

    public List<ProductItem> remove() {
        if (productList.size() > 0) {
            productList.remove(productList.size() - 1);
        }
        return productList;
    }

    public Long getId() {
        return id;
    }
    public int getQuantity() {
        return productList.size();
    }

    public int setQuantity(int quantity){
        if (quantity > productList.size()) {
            int delta = quantity - productList.size();
            for (int i = 0; i < delta; i++) {
                productList.add(new ProductItem());
            }
        } else if (productList.size() > quantity) {
            int delta = productList.size() - quantity;
            for (int i = 0; i < delta; i++) {
                productList.remove(productList.size() - 1);
            }
        }

        return getQuantity();
    }

    public ProductDescription getProductDescription() {
        return productDescription;
    }
}
