package ch.wipfli.kopfsalat.backend.exception;

import static ch.wipfli.kopfsalat.backend.exception.ExceptionMessage.AUTHENTICATION_TOKEN_EXPIRED;

public class AccountExpiredException extends BaseException {
    public AccountExpiredException() {
        super(AUTHENTICATION_TOKEN_EXPIRED);
    }
}
