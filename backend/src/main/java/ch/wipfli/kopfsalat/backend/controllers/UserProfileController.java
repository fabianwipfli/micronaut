package ch.wipfli.kopfsalat.backend.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;

import ch.wipfli.kopfsalat.backend.dtos.ConfirmationTokenData;
import ch.wipfli.kopfsalat.backend.exception.ContentNotValidException;
import ch.wipfli.kopfsalat.backend.exception.EmailNotValidException;
import ch.wipfli.kopfsalat.backend.exception.TokenNotValidException;
import ch.wipfli.kopfsalat.backend.exception.UserAlreadyExistsException;
import ch.wipfli.kopfsalat.backend.dtos.BenutzerData;
import ch.wipfli.kopfsalat.backend.entities.Location;
import ch.wipfli.kopfsalat.backend.entities.UserProfile;
import ch.wipfli.kopfsalat.backend.exception.UserNotExistsException;
import ch.wipfli.kopfsalat.backend.service.UserProfileService;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Put;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;

@Controller("/api/profile")
@Transactional
public class UserProfileController {

    @Inject
    private UserProfileService userProfileService;

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Post("/confirm")
    public BenutzerData confirm(@Body ConfirmationTokenData token) throws TokenNotValidException {
        final UserProfile result = userProfileService.confirm(token.getToken());
        return new BenutzerData(result);
    }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Get("/")
    public List<BenutzerData> getProfiles(@Nullable @QueryValue(value = "email") String[] emails, @Nullable Principal principal) throws UserNotExistsException {
        final List<BenutzerData> result = new ArrayList<>();
        if (principal != null) {
            result.add(new BenutzerData(userProfileService.findById(principal.getName())));
        } else if (emails == null) {
            final List<UserProfile> profiles = userProfileService.findAll();
            for (UserProfile item : profiles) {
                result.add(new BenutzerData(item));
            }
        }
        if (emails != null) {
            for (String email : emails) {
                try {
                    result.add(new BenutzerData(userProfileService.findById(email)));
                } catch (UserNotExistsException e) {
                    //do nothing
                }
            }
        }
        return result;
    }

    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Put("/")
    public BenutzerData updateCurrentProfile(Principal principal, @Valid @Body BenutzerData benutzerData) throws UserNotExistsException, ContentNotValidException {
        final UserProfile profile = userProfileService.findById(principal.getName());
        Location location = profile.getLocation();
        if (location == null) {
            location = new Location();
        }
        location.setTown(benutzerData.getCity());
        location.setPostcode(benutzerData.getPlz());
        location.setStreet(benutzerData.getStreet());
        location.setLatitude(benutzerData.getLatitude());
        location.setLongitude(benutzerData.getLongitude());
        profile.setLocation(location);
        profile.setFirstname(benutzerData.getFirstname());
        profile.setName(benutzerData.getName());
        profile.setDescription(benutzerData.getDescription());
        profile.setImage(benutzerData.getImage().getBytes());
        if (benutzerData.getPassword() != null && !benutzerData.getPassword().isEmpty()) {
            profile.setPassword(benutzerData.getPassword());
        }
        userProfileService.update(profile);
        benutzerData.setPassword(null);
        return benutzerData;
    }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Post("/")
    public BenutzerData register(@Valid @Body BenutzerData benutzerData, HttpRequest request) throws UserAlreadyExistsException, ContentNotValidException, EmailNotValidException {
        final UserProfile user = new UserProfile();
        user.setPassword(benutzerData.getPassword());
        user.setEmail(benutzerData.getEmail());
        user.setName(benutzerData.getName());
        user.setFirstname(benutzerData.getFirstname());
        user.setDescription(benutzerData.getDescription());
        user.setImage(benutzerData.getImage().getBytes());
        final Location location = new Location();
        location.setLatitude(benutzerData.getLatitude());
        location.setLongitude(benutzerData.getLongitude());
        location.setPostcode(benutzerData.getPlz());
        location.setTown(benutzerData.getCity());
        location.setStreet(benutzerData.getStreet());
        user.setLocation(location);
        userProfileService.register(user, request.getUri().getPath());
        benutzerData.setPassword(null);
        return benutzerData;
    }
}
