package ch.wipfli.kopfsalat.backend.exception;

import java.util.List;

import ch.wipfli.kopfsalat.backend.dtos.FormValidation;

public class FormValidationException extends RuntimeException {
    private List<FormValidation> formValidationList;

    public FormValidationException(String message, List<FormValidation> formValidationList) {
        super(message);
        this.formValidationList = formValidationList;
    }

    public List<FormValidation> getFormValidationList() {
        return formValidationList;
    }

    public void setFormValidationList(List<FormValidation> formValidationList) {
        this.formValidationList = formValidationList;
    }
}
