package ch.wipfli.kopfsalat.backend.controllers;

import java.net.MalformedURLException;
import java.net.URL;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import ch.wipfli.kopfsalat.backend.dtos.LoginData;
import ch.wipfli.kopfsalat.backend.dtos.TokenData;
import ch.wipfli.kopfsalat.backend.dtos.BenutzerData;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;

@MicronautTest
@Transactional
public class UserProfileControllerTest {
    @Inject
    private EmbeddedServer server;

    @Test
    public void testRegister() throws MalformedURLException {
        HttpClient client = HttpClient.create(new URL("http://" + server.getHost() + ":" + server.getPort()));
        BenutzerData benutzerData = new BenutzerData();
        benutzerData.setEmail("fabianwipfli@gmx.ch");
        benutzerData.setPassword("12345678");
        benutzerData.setFirstname("John");
        benutzerData.setName("Smith");
        benutzerData.setImage("");
        benutzerData.setCity("Mollis");
        benutzerData = client.toBlocking().retrieve(HttpRequest.POST("/api/profile", benutzerData), BenutzerData.class);
        Assertions.assertNotNull(benutzerData);
    }

    @Test
    public void testFindByIdUsingJWTToken() throws MalformedURLException {
        final HttpClient client = HttpClient.create(new URL("http://" + server.getHost() + ":" + server.getPort()));
        final LoginData loginData = new LoginData();
        loginData.setUsername("fabianwipfli@gmx.ch");
        loginData.setPassword("12345678");
        final TokenData token = client.toBlocking().retrieve(HttpRequest.POST("/login", loginData), TokenData.class);
        Assertions.assertNotNull(token);
        /*inal BenutzerData benutzer = client.toBlocking()
                .retrieve(HttpRequest.GET("/api/profile").bearerAuth(token.getAccessToken()), BenutzerData.class);
        Assertions.assertNotNull(benutzer);*/
    }

    /*@Test
    public void testAddFailed() throws MalformedURLException {
        HttpClient client = HttpClient.create(new URL("http://" + server.getHost() + ":" + server.getPort()));
        Person person = new Person();
        person.setFirstName("John");
        person.setLastName("Smith");
        person.setAge(33);
        person.setGender(Gender.MALE);
        Assertions.assertThrows(HttpClientResponseException.class,
                () -> client.toBlocking().retrieve(HttpRequest.POST("/secure/persons", person).basicAuth("scott", "scott123"), Person.class),
                "Forbidden");
    }

    @Test
    public void testFindById() throws MalformedURLException {
        HttpClient client = HttpClient.create(new URL("http://" + server.getHost() + ":" + server.getPort()));
        Person person = client.toBlocking()
                .retrieve(HttpRequest.GET("/secure/persons/1").basicAuth("scott", "scott123"), Person.class);
        Assertions.assertNotNull(person);
    }*/
}
